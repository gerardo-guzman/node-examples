/*
PATRON OBSERVADOR!
*/ 
'use strict'

var EventEmitter = require('events').EventEmitter;
var pub = new EventEmitter()

// equivalente a addListener
pub.on('myevent', function(message){
    console.log(message);
});

pub.once('myevent', function(message){
    console.log('Se emite la primera vez');
});

// COMO EMITIRLOS   ?

pub.emit('myevent', 'Soy un emisor de Eventos');
pub.emit('myevent', 'Volviendo a emitir');

// REMOVER EVENTO

pub.removeAllListeners('myevent');

