'use strict'

function singleThread(){
    console.log('---------------------------------------');
    console.log(' EL PROCESO DE NODE ');
    console.log('titulo .............' + process.title);
    console.log('Id del proceso .....' + process.pid);
    console.log('Directorio Node ....' + process.execPath);
    console.log('Directorio actual ..' + process.cwd());
    console.log('version Node .......' + process.version);
    console.log('Tiempo activo ......' + process.uptime());
    console.log('Plataforma .........' + process.platform);
    console.log('Arquitectura .......' + process.arch);
    console.log('Process Args .......' + process.argv);
    console.log('---------------------------------------');
}

singleThread()