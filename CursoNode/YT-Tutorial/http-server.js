'use strict'

// const http = require('http');

// const hostname = '127.0.0.1';
// const port = 3000;

// const server = http.createServer((request, response) => {
//   response.statusCode = 200;
//   response.setHeader('Content-Type', 'text/plain');
//   response.end('Hello World');
// });

// server.listen(port, hostname, () => {
//   console.log(`Server running at http://${hostname}:${port}/`);
// });

const http = require('http');

const hostname = 'localhost';
const port = 3000;

function webServer(req, res){
    res.writeHead(200,{'Content-Type' : 'text/html'})
    res.end('<h1>HOLA MUNDO</h1>') 
}

const server = http.createServer(webServer);

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});