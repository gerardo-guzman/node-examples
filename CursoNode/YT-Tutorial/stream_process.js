'use strict'

var stdin = process.stdin,
    stdout = process.stdout,
    person = {
        name: null,
        age : 0
    }

function saveAge(age){
    person.age = age;
    if(person.age >= 18){
        stdout.write(person.name + 'Es mayor de Edad: ' + person.age + ' años\n')
    }else{
        stdout.write(person.name + 'Es menor de Edad: ' + person.age + ' años\n')
    }

    process.exit();
}

function saveName(name){
    person.name = name;
    var question = 'hola ' + person.name + ', Qué edad tienes?';
    quiz(question, saveAge);
}

function quiz(question, callback){

    stdin.resume() // -> leer de la linea de comandos
    stdout.write(question + ':')
    stdin.once('data', function(res){
        callback(res.toString().trim() )
    })

}

stdin.setEncoding('utf8')
quiz('¿Cómo te llamas?', saveName)