/*  STREAMS -> chorro de información
        3 tipos: Lectura / Escritura/ Duplex
        Son instancias de EventEmitter
        Acceso asíncrono
        Objetvo Stream
*/
var fs = require('fs'),
    readStream = fs.createReadStream('assets/nombres.txt'),
    writeStream = fs.createWriteStream('assets/nombres_copia.txt')
// pipe permite crear proceso de lectura y escritura

readStream.pipe(writeStream)

readStream.on('data', function(chunk){
    console.log('He leído: ', chunk.length, ' caracteres')
})

readStream.on('end', function(){
    console.log('termine de leer')
})

    
