// setInterval(/*funcion callback*/()=>{
//     console.log('HOLA CALLBACK');
// }, 3000);

let getUsuarioById = (id, callback) => {
    let usuario = {
        nombre: 'Jerry',
        id
    }

    if(id === 17){
        callback(`El usuario con id ${id}, no existe en la BD`);
    }else callback(null, usuario);
}

getUsuarioById(17, (err, usuario)=>{

    if(err) return console.log(err);
     
    console.log('DataBase user ', usuario);
});

