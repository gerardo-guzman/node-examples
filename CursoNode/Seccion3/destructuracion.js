let deadpool = {
    nombre: 'Wade',
    apellido: 'Wilson',
    poder: 'Regeneración',
    getNombre: function(){
        return `${this.nombre} ${this.apellido} - Poder: ${this.poder}`;
    }
}

let {
    nombre: firstname, apellido, poder
} = deadpool;

console.log(firstname , apellido, poder);

