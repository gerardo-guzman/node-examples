let empleados = [{
    id:1,
    nombre: 'jerry'
},{
    id: 2,
    nombre: 'mely'
},{
    id: 3,
    nombre: 'alex'
}
];

let salarios =[{
    id:1,
    salario:1000
},{
    id:2,
    salario: 2000
}];

let getEmpleado = (id, callback) => {

    let empleadoDB = empleados.find(empleado => empleado.id === id);

    if(!empleadoDB){
        callback(`No existe un empleado con el id: ${id}`)
    }else callback(null, empleadoDB);
     
    // co nsole.log(empleadoDB );
}

let getSalario = (empleado, callback) =>{
    
    let salarioDB = salarios.find(salario => salario.id === empleado.id);

    if(!salarioDB){
        callback(`El empleado id: ${empleado.id} No tiene salario o no existe`);
    }else callback(null, {nombre: empleado.nombre, salario: salarioDB.salario});

}

getEmpleado(3, (err, empleado)=>{

    if(err) return console.log(err);

    getSalario(empleado, (err, resp)=>{
        if(err) return console.log(err);

        console.log(`El salario de ${resp.nombre} son:  $${resp.salario} pesos`);
    })
});

