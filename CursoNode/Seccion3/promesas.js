let empleados = [{
    id:1,
    nombre: 'jerry'
},{
    id: 2,
    nombre: 'mely'
},{
    id: 3,
    nombre: 'alex'
}
];

let salarios =[{
    id:1,
    salario:1000
},{
    id:2,
    salario: 2000
}];

let getEmpleado = (id) => {

    return new Promise((resolve, reject) =>{
        let empleadoDB = empleados.find(empleado => empleado.id === id);

        if(!empleadoDB){
            reject(`No existe un empleado con el id: ${id}`);
        }else resolve(empleadoDB);
    });
}

let getSalario = (empleado) => {
    return new Promise((resolve,reject)=>{
        let salarioDB = salarios.find(salario => salario.id === empleado.id);

        if(!salarioDB){
            reject(`El usuario id: ${empleado.id} No tiene salario`);
        }else resolve(`El usuario ${empleado.nombre} tiene sueldo de: $${salarioDB.salario}`);
    });
}

getEmpleado(5).then(result => {
    
    return getSalario(result);

}).then(resp =>{
    console.log(resp);    
}).catch(err =>{
    console.log(err);
})

