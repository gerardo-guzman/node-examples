let empleados = [{
    id:1,
    nombre: 'jerry'
},{
    id: 2,
    nombre: 'mely'
},{
    id: 3,
    nombre: 'alex'
}
];

let salarios =[{
    id:1,
    salario:1000
},{
    id:2,
    salario: 2000
}];

let getEmpleado = async(id) => {

    
        let empleadoDB = empleados.find(empleado => empleado.id === id);

        if(!empleadoDB){
            throw new Error(`No existe un empleado con el id: ${id}`);
        }else return empleadoDB;
}

let getSalario = async(empleado) => {
    
        let salarioDB = salarios.find(salario => salario.id === empleado.id);

        if(!salarioDB){
            throw new Error(`El usuario id: ${empleado.id} No tiene salario`);
        }else return salarioDB;
}

let getInformacion = async(id) => {
    
    let empleado = await getEmpleado(id);
    let resp = await getSalario(empleado);
    
    return `${ empleado.nombre } tiene un salario de $${resp.salario}`;
}

getInformacion(1)
.then(mssg => console.log(mssg))
.catch(e => console.log(e));
