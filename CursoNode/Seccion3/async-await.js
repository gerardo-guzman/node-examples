
/*
let getNombre = async() =>{

    // throw new Error('No existe el usuario');
    return 'JerryStk';
}

getNombre().then(nombre =>{
    console.log(nombre);
}).catch(e =>{
    console.log('ERROR EN ASYNC: ', e);
});*/

let getNombre = () =>{

    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            resolve('Jerry');
        },3000);
    });  
}

let saludo = async() =>{

    let nombre = await getNombre();
    return `Hola ${nombre}`;
}

saludo().then(mensaje =>{
    console.log(mensaje);
});